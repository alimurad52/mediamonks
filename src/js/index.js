let carousel = document.getElementById("carousel-container");
let slides = document.getElementsByClassName("slide");
let dots = document.getElementsByClassName("dot");

let prevArrow = document.querySelector(".prev");
let nextArrow = document.querySelector(".next");

let currentSlide = 1;
let index;

setTimeout(() => {
    document.getElementById('landing-page').classList.add('fadeout');
    setTimeout(() => {
        //to hide and show the landing page container and carousel container
        document.getElementById('carousel-container').classList.remove('display-none');
        document.getElementById('landing-page').classList.add('display-none');
        document.getElementById('carousel-container').classList.add('fadein');
        document.getElementById('dots-container').classList.remove('display-none');
    }, 1500)

}, 1700);
// to iterate to the next slide
const next = () => {
    showSlide(currentSlide += 1);
};
// to go back to the prev slide
const prev = () => {
    showSlide(currentSlide -= 1 );
};
// add the relevant classes to the next/previous slide
const showSlide = (n) => {
    prevArrow.style.display = "block";
    nextArrow.style.display = "block";
    // to hide the right arrow when its the last slide
    if (n >= slides.length) {
        currentSlide = slides.length;
        nextArrow.style.display = "none";
    }
    //to hide the left arrow when active slide is the first one
    if (n <= 1) {
        currentSlide = 1;
        prevArrow.style.display = "none";
    }
    //to remove the current active slide class
    for (index = 0; index < slides.length; index++) {
        slides[index].classList.remove("active");
    }
    //to remove active class to dots once the slide is changed
    for (index = 0; index < dots.length; index++) {
        dots[index].classList.remove("active-dot");
        dots[index].classList.remove("zoom-in");
    }
    //to add class active to the next slide
    slides[currentSlide - 1].classList.add("active");
    //to add active calsses to the next dot
    dots[currentSlide - 1].classList.add("active-dot");
    dots[currentSlide - 1].classList.add("zoom-in");
    //to change the background according to the current slide
    if (currentSlide < slides.length) {
        carousel.style.backgroundPosition = (currentSlide - 1) * 12.5 + "% top";
        slides[currentSlide - 1].classList.add("fadein");
    } else {
        //to add transition to the content on the last page
        carousel.style.backgroundPosition = "114.25% top";
        slides[currentSlide - 1].querySelector(".content").classList.add("slidein");
        slides[currentSlide - 1].querySelector(".counter").classList.add("slidein");
    }
};
//onclick when user clicks on dots to jump to that slide
const onClickDot = (n) => {
    //check if selected dot is not the currently active slide
    if (n != currentSlide) {
        showSlide((currentSlide = n));
    }
};
